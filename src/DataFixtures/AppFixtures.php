<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker::create('fr_FR');

        //Create 10 Authors
        $authors = [];
        for ($i = 0; $i < 10; $i++) {
            $author = new Author();
            $author->setName($faker->name);
            $manager->persist($author);
            $authors[] = $author;
        }

        //Create 10 Publishers
        $publishers = [];
        for ($i = 0; $i < 10; $i++) {
            $publisher = new Publisher();
            $publisher->setName($faker->company);
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        //Create Status
        $status =[];
        foreach (['to-read', 'reading', 'read'] as $value){
            $oneStatus = new Status();
            $oneStatus->setName($value);
            $manager->persist($oneStatus);
            $status[] = $oneStatus;
        }

        //Create 100 Books
        $books = [];
        for ($i = 0; $i < 100; $i++) {
            $book = new Book();
            $book->setGoogleBooksId($faker->uuid);
            $book->setTitle($faker->sentence);
            $book->setSubtitle($faker->sentence);
            $book->setPublishDate($faker->dateTime);
            $book->setDescription($faker->text);
            $book->setIsbn10($faker->isbn10);
            $book->setIsbn13($faker->isbn13);
            $book->setPageCount($faker->numberBetween(100, 1000));
            $book->setSmallThumbnail('https://picsum.photos/200/300');
            $book->setThumbnail('https://picsum.photos/100/150');
            $book->addAuthor($faker->randomElement($authors));
            $book->addPublisher($faker->randomElement($publishers));
            $manager->persist($book);
            $books[] = $book;
        }

        //Create 10 Users
        $users = [];
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($faker->password);
            $user->setPseudo($faker->userName);
            $manager->persist($user);
            $users[] = $user;
        }

        //Create 10 Userbook by User
        foreach ($users as $user) {
            for ($i = 0; $i < 10; $i++) {
                $userbook = new UserBook();
                $userbook->setReader($user);
                $userbook->setStatus($faker->randomElement($status));
                $userbook->setRating($faker->numberBetween(0, 5));
                $userbook->setComment($faker->text);
                $userbook->setBook($faker->randomElement($books));
                $userbook->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime));
                $userbook->setUpdateAt(\DateTimeImmutable::createFromMutable($faker->dateTime));
                $manager->persist($userbook);
            }
        }

        $manager->flush();
    }
}
